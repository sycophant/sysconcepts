/** Exploring command line paramters in C++
	argc is an integer listing the number of parameters
	argv is a cstring type array containing the parameters
	argv[0] should be the name of the program
*/
#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include "spin.h"


int shared = 0;
int numIterations = 20;
int sleepTime = 10;

void foo (int x, std::string label)
{
	int num;
	for (int i = 0; i < x; i++)
	{
		while (!getLock()) { spin(0.5); }
		std::cout << label << " reading shared = " << shared << std::endl;
		num = shared;
		std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
		num++;
		shared = num;
		std::cout << "Thread1 writing shared = " << shared << std::endl;
		releaseLock();
	}
}


int main(int argc, char * argv[])
{
	std::thread th1(foo, numIterations, "Thread 1");
	std::cout << "Thread launched" << std::endl;
	int num;
	foo(numIterations, "main");
	th1.join();
	std::cout << "Thread complete" << std::endl;
	char ch = getchar();

	return 0;
}
