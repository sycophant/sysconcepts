/** 
 Life with header files
*/
#include <iostream>
// #include <unistd.h>
#include <time.h>

#include "spin.h"

int lock = 0;

bool getLock() {
	if (lock > 0)
	{
		return false;
	}
	lock = 1;
	return true;
}

void releaseLock() {
	lock = 0;
}

void spin(int seconds) {
	clock_t endwait;
	endwait = clock() + seconds * CLOCKS_PER_SEC;
	while (clock() < endwait) {};
}


