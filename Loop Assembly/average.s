	.file	"average.cpp"	#file ehader
	.text
	.globl	s
	.section	.rodata
.LC0:
	.string	"This is some data"	#allocation for the string and assignment
	.data
	.align 8
	.type	s, @object
	.size	s, 8
s:
	.quad	.LC0
	.globl	z
	.align 4
	.type	z, @object
	.size	z, 4
z:
	.long	7			#allocation for global z of value 7
	.text
	.globl	main 		#allocation for global main function
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp	#push to base pointer
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#moving stack pointer to base pointer location
	.cfi_def_cfa_register 6
	movl	$5, -4(%rbp)	#moving address of value 5 from location given by pinter offset
	movl	$9, -8(%rbp)	#same as above, with different offsets
	movl	-4(%rbp), %edx	#temporary registers for addition
	movl	-8(%rbp), %eax
	addl	%eax, %edx
	movl	z(%rip), %eax	#after addition is done, moving to instruction pointer
	leal	(%rdx,%rax), %ecx	#division happening, moving result to temporary register
	movl	$1431655766, %edx	#more temp registers created to handle longs
	movl	%ecx, %eax
	imull	%edx		#multiplies integers
	movl	%ecx, %eax
	sarl	$31, %eax	#bit shift to the right by 31
	subl	%eax, %edx	#subtracting a from b
	movl	%edx, %eax
	movl	%eax, -12(%rbp)		#moves value in address to temp register
	movl	$0, %eax
	popq	%rbp	#pops base pointer off stack
	.cfi_def_cfa 7, 8
	ret 		#return value
	.cfi_endproc
.LFE0:	#compiler info
	.size	main, .-main
	.ident	"GCC: (SUSE Linux) 7.5.0"
	.section	.note.GNU-stack,"",@progbits
