// Exploring assembly language
#include <string>

#define Y 3
std::string progname = "loop.cpp";

int main()
{
    int x = 5;
    int y = Y;
	  for (int i = 0; i < Y; i++) {
		  x = x + i;
	}

  return 0;
}
