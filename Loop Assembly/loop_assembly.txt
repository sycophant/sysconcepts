Previously, we looked at compiling a simple program and building the object code for it.

A couple of potentially helpful links.
https://www.recurse.com/blog/7-understanding-c-by-learning-assembly 
https://cseweb.ucsd.edu/classes/sp10/cse141/pdf/02/S01_x86_64.key.pdf


Review of loop.cpp
Gdb disassembly:

g++ -O0 -o loop loop.cpp
gdb loop

(gdb) break main
(gdb) run
(gdb) next 2 (probably not needed for us)
(gdb) disassemble

Loop:

%rbp is the base pointer, which points to the base of the current stack frame 
%rsp is the stack pointer, which points to the top of the current stack frame. 
%rbp always has a higher value than %rsp because the stack starts at a high memory address and grows downwards.

Dump of assembler code for function main:
Preamble: Push old base pointer onto the stack. Then copy stack pointer to base pointer. Base pointer now points to the base of main's stack frame.
   0x0000000000400926 <+0>:     push   %rbp
   0x0000000000400927 <+1>:     mov    %rsp,%rbp
   

edi register is a general purpose register often used as a pointer. It likely holds our argv.
=> 0x000000000040092a <+4>:     mov    %edi,-0x14(%rbp)

rsi is often used as a source for data copies. Probably argc.
   0x000000000040092d <+7>:     mov    %rsi,-0x20(%rbp)
   
Copy 5 onto the stack (x)
   0x0000000000400931 <+11>:    movl   $0x5,-0xc(%rbp)
   
Copy 3 onto the stack (Y is nowhere to be seen)
   0x0000000000400938 <+18>:    movl   $0x3,-0x4(%rbp)
   
Set up the loop. Place 0 in the stack where our counter goes
   0x000000000040093f <+25>:    movl   $0x0,-0x8(%rbp)
   
The cmpl command computes 2 - contents of the counter and sets flags
   0x0000000000400946 <+32>:    cmpl   $0x2,-0x8(%rbp)
   
Jump greater (jg) will branch to the bottom of the loop if the previous comparison is true
   0x000000000040094a <+36>:    jg     0x400958 <main+50>
   
Move counter value to eax
   0x000000000040094c <+38>:    mov    -0x8(%rbp),%eax
   
Add contents of #eax to x
   0x000000000040094f <+41>:    add    %eax,-0xc(%rbp)

Add 1 to counter
   0x0000000000400952 <+44>:    addl   $0x1,-0x8(%rbp)
   
Jump to the top of the loop
   0x0000000000400956 <+48>:    jmp    0x400946 <main+32>
   
Move return value to %eax
   0x0000000000400958 <+50>:    mov    $0x0,%eax

Pop the old base pointer and store in %rbp
   0x000000000040095d <+55>:    pop    %rbp
   
Jump to the return address and pop it off the stack
   0x000000000040095e <+56>:    retq
End of assembler dump.

Note that all of our variable names are gone. They are for our benefit. The machine only needs the addresses.

