#include <iostream>
#include <cmath>
#include <iomanip>

#include "shape.h"

//calc methods

Circle::Circle(Point p, double r):_center(p),_radius(r) {}

double Circle::circumference()  { return 2.0 * M_PI * _radius; }

double Circle::area() { return M_PI* _radius*_radius; }

Rectangle::Rectangle(Point anchor, double width, double height) {}

Rectangle::Rectangle(Point p1, Point p2)
{
	width = std::abs(p2.getX() - p1.getX());
	height = std::abs(p2.getY() - p1.getY());
}

double Rectangle::circumference() {return (2.0 * width) + (2.0 * height);}

double Rectangle::area() {return width * height;}

Square::Square(Point center, double length)
{
	_center = center;
	_length = length;
}

double Square::circumference() {return 4.0 * _length;}

double Square::area() {return _length * _length;}

//print methods

void Shape::print() {std::cout << "No shape to print" << std::endl;}
void Point::print() {std::cout << "Point (" << getX() << "," << getY() << ")" << std::endl;}
void Circle::print() 
{
	std::cout << "Circle" << std::endl;
	std::cout << std::setw(20) << "Circumference" << std::setw(20) << "Area" << std::setw(20) << "Radius" <<  std::endl;
	std::cout << std::setw(20) << circumference() << std::setw(20) << area() << std::setw(20) << radius() <<  std::endl;

}
void Rectangle::print()
{
	std::cout << "Rectangle" << std::endl;
	std::cout << std::setw(20) << "Perimeter" << std::setw(20) << "Area" << std::setw(20) << "Width" << std::setw(20) << "Height" << std::endl;
	std::cout << std::setw(20) << circumference() << std::setw(20) << area() << std::setw(20) << getWidth() << std::setw(20) << getHeight() <<  std::endl;
}
void Square::print()
{
	std::cout << "Square" << std::endl;
	std::cout << std::setw(20) << "Perimeter" << std::setw(20) << "Area" << std::setw(20) << "Length of Side"  << std::endl;
	std::cout << std::setw(20) << circumference() << std::setw(20) << area() << std::setw(20) << lengthOfSide() <<  std::endl;
}
//end print methods

int main()
{
	std::cout << "Starting shape test" << std::endl;
	Point p1(0, 0);
	Point p2(1, 1);
	p1.print();
	p2.print();

	Shape* ptr = new Circle(p1, 5);
	Shape* ptr2 = new Rectangle(p1, p2);
	Shape* ptr3 = new Square(p1, 5);
	Circle* cptr = (Circle*)ptr;
	Rectangle* rptr = (Rectangle*)ptr2;
	Square* sptr = (Square*)ptr3;

	ptr->print();
	cptr->print();
	rptr->print();
	sptr->print();
	
	delete ptr;
	return 0;
}