#pragma once
/**
* Starter code for CSCI 342 class assignment
* Author: Chris Branton
* Created 2019-10-09
* Updated 2020-11-10
*
*/

#include <cmath>
#include <iomanip>

/**
* Base shape class. Should be abstract
*/
class Shape
{
	public:
		virtual double area() = 0;
		virtual double circumference() = 0;
		virtual ~Shape() = default;
		void print();
};

class Point
{
private:
	double xCoord, yCoord;
public:
	Point(): xCoord(0), yCoord(0) {}
	Point(double x, double y) :xCoord(x), yCoord(y) {}
	double getX() { return xCoord; }
	double getY() { return yCoord; }
    void print();
	virtual ~Point() = default;
};

class Circle : public Shape
{
public:
	Circle(Point, double);
	Point center() { return _center; }
	virtual double radius() { return _radius; }
	virtual double area();
	virtual double circumference();
	virtual ~Circle() = default;
	void print();

private:
	Point _center;
	double _radius;
};

class Rectangle: public Shape
{
private:
	Point p1, p2;
	double width;
	double height;

public:
	Rectangle() = default;
	Rectangle(Point p1, Point p2);
	Rectangle(Point anchor, double width, double height);
	Point point1() { return p1; }
	Point point2() { return p2; }
	virtual double area();
	virtual double circumference();
	double getHeight () { return height; }
	double getWidth () { return width; }
	//print method
	void print();
	virtual ~Rectangle() = default;
};

class Square: public Rectangle
{
public:
	//constructors
	Square() = default;
	Square(Point, double);
	Point center() { return _center; }
	double lengthOfSide() {return _length;};
	virtual double area();
	virtual double circumference();
    void print();
	virtual ~Square() = default;
private:
	double _length;
	Point _center;
};